# ![Wildfire Logo](Design/Logo/Logo.png)
<p align="center"><i>Exploration, Research, Discovery</i> - a <a href="http://www.nexusmods.com/skyrim/mods/64798">Campfire</a>-based mod for Skyrim</p>

### Main Ideas
 - Exploration (dungeons & random wandering) rewards you with Relics
 - Different types of Relics yield different rewards
 - Which ones you find depends on your skills (e.g. mages will get more magic-related ones)
 - Research (at a camp) needed to unlock these secrets
 - As you level your skills (perk trees) up, you'll gain access to more abilities, more relics, etc.
 - Different relic sizes: minor (leveled loot), normal and ancient (hand-placed)

### Perk Trees
 - Every perk tree is hidden and has to be unlocked first
 - **Archaelogy**
     - Unlocked by: Finding your first Relic
     - Improved by: Finding and researching more Relics
     - Perks: Unlocks studying and finding other types of Relics and improves the benefits they give you when researched
 - **Arcana**
     - Unlocked by: Researching your first Arcana-specific Relic
     - Improved by: Researching more Arcana-specific Relics
     - Perks: Improvements to magical abilities, new spells and powers
 - **Reflexes**
     - Unlocked by: Researching your first Reflexes-specific Relic
     - Improved by: Researching more Reflexes-specific Relics
     - Perks: Bonuses to thievery, sneaking and dodging
 - **Strength**
     - Unlocked by: Researching your first Strength-specific Relic
     - Improved by: Researching more Strength-specific Relics
     - Perks: Bonuses to combat, health and resistances
 - **Exploration**
     - Unlocked by: Finding a significant landmark (i.e. Blackreach, Throat of the World)
     - Improved by: Discovering new places
     - Perks: Provides general navigation and exploration buffs when traveling to many different areas of the map
 - **More to come...**

### License
    Copyright 2017 Infernio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
